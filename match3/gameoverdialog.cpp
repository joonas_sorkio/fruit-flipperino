/*
#############################################################################
# COMP.CS.110 Programming 2: Autumn 2020                                    #
# Project 4: Match3 (fruit_flip)                                            #
# File: gameoverdialog.cpp                                                  #
# Author: Joonas Sorkio (joonas.sorkio@tuni.fi)                             #
# Description: A gameoverdialog is called from mainwindow functions         #
#              if the game end condition is met. Freezes the mainwindow     #
#              until it is closed.                                          #
# Notes: * See gameoverdialog.hh for header.                                #
#        * ui elements laid out in gameoverdialog.ui                        #
#        * Fruit images provided by Freepik (see license.txt)               #
#############################################################################
*/

#include "gameoverdialog.hh"
#include "ui_gameoverdialog.h"

gameOverDialog::gameOverDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::gameOverDialog) {

    ui->setupUi(this);
    this->setWindowTitle("RIP");
}

gameOverDialog::~gameOverDialog() {
    delete ui;
}

void gameOverDialog::on_closeButton_clicked() {
    // Closing the dialog, game board is ready for another go.
    this->close();
}

void gameOverDialog::setPoints(int newPoints) {
    // Set points with a new value from MainWindow.
    points = newPoints;
    // Printing the final points total to the user.
    QString pointsStr = QString::number(points);
    ui->pointsLabel->setText(POINTS_LABEL + pointsStr);
}

void gameOverDialog::setReason(QString newReason) {
    // Sets a new reason for ending the game.
    reason = newReason;
    // Printing the reason to the user.
    ui->reasonLabel->setText(reason);
}

