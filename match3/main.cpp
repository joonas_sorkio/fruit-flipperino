/*
#############################################################################
# COMP.CS.110 Programming 2: Autumn 2020                                    #
# Project4: Match3 (fruit_flip)                                             #
# File: main.cpp                                                            #
# Author: Joonas Sorkio (joonas.sorkio@tuni.fi)                             #
# Description: main-class for launching the graphical user interface        #
#        of the fruit flip game.                                            #
# Notes: Bulk of the code can be found in mainwindow.hh, mainwindow.cpp,    #
#        gameoverdialog.hh and gameoverdialog.cpp                           #
#        * Fruit images provided by Freepik (see license.txt)               #
#############################################################################
*/

#include "mainwindow.hh"

#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
