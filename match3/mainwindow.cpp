﻿/*
#############################################################################
# COMP.CS.110 Programming 2: Autumn 2020                                    #
# Project4: Match3 (fruit_flip)                                             #
# File: mainwindow.cpp                                                      #
# Author: Joonas Sorkio (joonas.sorkio@tuni.fi)                             #
# Description: User interface and functions for the fruit flip game.        #
# Notes: * See mainwindow.hh for function documentation                     #
#        * Game board, fruit squares and corresponding buttons are laid out #
#          manually.                                                        #
#        * Other user interface elements are laid out in mainwindow.ui.     #
#        * Fruit images provided by Freepik (see license.txt)               #
#############################################################################
*/

#include "mainwindow.hh"
#include "ui_mainwindow.h"
#include "gameoverdialog.hh"

#include <QKeyEvent>
#include <QDebug>
#include <QPixmap>
#include <QWidget>
#include <vector>
#include <string>
#include <QPushButton>
#include <math.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow) {

    ui->setupUi(this);
    // Setting an appropriate title for the window
    this->setWindowTitle("Match 3: The Flipperino Of The Fruitterino");

    // Graphics scene which contains the fruit squares.
    scene_ = new QGraphicsScene(this);

    // Setting the geometry of the graphicsview.
    ui->graphicsView->setGeometry(LEFT_MARGIN, TOP_MARGIN,
                                  BORDER_RIGHT + 2, BORDER_DOWN + 2);
    ui->graphicsView->setScene(scene_);
    // Setting a white background for the graphics view(game board/grid),.
    ui->graphicsView->setBackgroundBrush(QBrush(Qt::white, Qt::SolidPattern));
    // Setting the dimensions for the scene(game board/grid),
    scene_->setSceneRect(0, 0, BORDER_RIGHT - 1, BORDER_DOWN - 1);

    // Initializing the random engine and corresponding distribution.
    int seed = time(0);
    randomEng_.seed(seed);
    // Distribution range is the total number of fruits
    distr_ = std::uniform_int_distribution<int>(0, NUMBER_OF_FRUITS - 1);
    // Wiping out the first random number
    distr_(randomEng_);

    // Creating a new shared pointer to a Player -type struct.
    std::shared_ptr<Player> newPlayer = std::make_shared<Player>(Player{});
    player = newPlayer;

    // Initializing and connecting the timer to slot.
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(primaryTimerFunction()));
    // Timer data is displayed in two lcdNumber-objects by minutes and seconds
    ui->lcdNumberMin->display(3);
    ui->lcdNumberSec->display(0);

    //Filling the board with fruit squares.
    fillGrid();
}

MainWindow::~MainWindow() {
    delete ui;
}

/* PRIVATE FUNCTIONS PRIVATE FUNCTIONS PRIVATE FUNCTIONS PRIVATE FUNCTIONS
 * PRIVATE FUNCTIONS PRIVATE FUNCTIONS PRIVATE FUNCTIONS PRIVATE FUNCTIONS
 * PRIVATE FUNCTIONS PRIVATE FUNCTIONS PRIVATE FUNCTIONS PRIVATE FUNCTIONS
 * PRIVATE FUNCTIONS PRIVATE FUNCTIONS PRIVATE FUNCTIONS PRIVATE FUNCTIONS
 */

void MainWindow::drawFruit(QPushButton* fruitButton, int fruitNum) {

    // If the incoming fruitNum is marked as NO_FRUIT, return immediately.
    if (fruitNum == NO_FRUIT) {
        return;
    }
    // Initializing a vector of fruits
    const std::vector<std::string>
            fruits = {"cherries", "strawberry", "orange", "pear",
                      "bananas", "tomato", "grapes", "eggplant"};

    // Defining where the images can be found and what kind of images.
    const std::string PREFIX(":/");
    const std::string SUFFIX(".png");

    // Converting image (png) to a pixmap
    std::string filename = PREFIX + fruits.at(fruitNum) + SUFFIX;
    QPixmap image(QString::fromStdString(filename));

    // Fruit pixmap is set as the icon of the matching fruitButton
    QIcon fruitIcon(image);
    fruitButton->setIcon(fruitIcon);
    fruitButton->setIconSize(fruitButton->size());
}

void MainWindow::fillGrid() {
    // Setting left margin and top margin as the initial coordinates
    int x = LEFT_MARGIN;
    int y = TOP_MARGIN;
    // Index counter, values will be set as the Ids of fruit squares.
    int newId = 0;

    // New rows will be added until the bottom of the scene is reached.
    while (y < (TOP_MARGIN + (SQUARE_SIDE * ROWS))) {

        // Initiliazing an empty vector for the contents of the row.
        std::vector<std::shared_ptr<fruitSquare>> fruitRow = {};

        // New elements will be added until the right side limit is reached.
        while (x < (LEFT_MARGIN + (SQUARE_SIDE * COLUMNS))) {

            // Forming a new random fruit value with the random engine.
            int randomFruit = distr_(randomEng_);

            // New fruitbutton for the fruit square, connecting to a slot.
            QPushButton* fruitButton = new QPushButton(this);
            connect(fruitButton, &QPushButton::clicked, this,
                    &MainWindow::onfruitButtonClicked);

            // Setting the location and other characteristics for the button.
            fruitButton->setGeometry(x, y, SQUARE_SIDE, SQUARE_SIDE);
            fruitButton->setStyleSheet("background-color: rgb(215, 215, 215);");
            fruitButton->setFlat(true);
            fruitButton->setCheckable(true);
            fruitButton->setEnabled(false);
            // Corresponding fruit is drawn to the button (as a QICon).
            drawFruit(fruitButton, randomFruit);

             // Creating a new shared pointer for a fruit squaresquare.
            std::shared_ptr<fruitSquare> newfruitSquare =
                            std::make_shared<fruitSquare>(fruitSquare{});

            // Assigning the new square a fruit number, button and an id.
            newfruitSquare->fruitNum = randomFruit;
            newfruitSquare->fruitButton = fruitButton;
            newfruitSquare->id = newId;

            // Finally, the new square is pushed into the row vector.
            fruitRow.push_back(newfruitSquare);
            // Increasing Id index.
            newId++;
            // Adjusting for the next horizontal coordinate.
            x = x + SQUARE_SIDE;
         }
         // Pushing the current row into the gridVector.
         gridVector.push_back(fruitRow);
         // Horizontal coordinate is set back to the left margin.
         x = LEFT_MARGIN;
         // y coordinate is moved below by the length of one square.
         y = y + SQUARE_SIDE;
     }
     // If the board already has matches, the fruits will be redrawn.
     bool matches = checkForMatches(false);
     if (matches) {
        on_resetButton_clicked();
     }
}

bool MainWindow::checkForAdjacency(int& lastX, int& lastY,
                                   int& newX, int& newY) {

    // Return true if in the same column and within one row of each other.
    if (newX == lastX && newY - lastY == 1) {
        return true;
    }
    if (newX == lastX && newY - lastY == -1) {
        return true;
    }
    // Return true if in the same row and within one column of each other.
    if (newY == lastY && newX - lastX == 1) {
        return true;
    }
    if (newY == lastY && newX - lastX == -1) {
        return true;
    }
    // In any other case, it is deemed that the squares aren't adjacent.
    return false;
}

bool MainWindow::checkForMatches(bool enableRemove) {

    // Initializing an empty vector for keeping track of the matched squares.
    std::vector<std::shared_ptr<fruitSquare>> matched = {};
    // By default, we are assuming that no matches exist
    bool matchesFound = false;

    // Checking for horizontal matches row by row.
    for (int ind = 0; ind < ROWS; ind++) {

        for (int ind2 = 0; ind2 < COLUMNS - 2; ind2++) {
            // Check if the current fruit matches the next two in the column.
            if (gridVector[ind][ind2]->fruitNum != NO_FRUIT &&
                gridVector[ind][ind2]->fruitNum ==
                gridVector[ind][ind2+1]->fruitNum &&
                gridVector[ind][ind2+1]->fruitNum ==
                gridVector[ind][ind2+2]->fruitNum) {

                // Check if the 'matched' vector already contains the square.
                for (int ind3 = ind2; ind3 <= ind2+2; ind3++) {
                    // First, lets assume that no duplicates exist.
                    bool matchedContains = false;

                    // Comparing to the contents of the 'matched' vector.
                    for (int ind4 = 0; ind4 < (int)matched.size(); ind4++) {
                        // Match is found, moving to the next fruit square.
                        if(matched[ind4]->id == gridVector[ind][ind3]->id) {
                            matchedContains = true;
                            break;
                        }
                    }
                    // Duplicate not found, adding the current fruit square.
                    if (!matchedContains) {
                        matched.push_back(gridVector[ind][ind3]);
                    }
                }
                // Horizontal matches were found.
                matchesFound = true;
            }
        }
    }
    // Checking for vertical matches column by column.
    for (int ind = 0; ind < COLUMNS ; ind++) {

        for (int ind2 = 0; ind2 < ROWS - 2; ind2++) {
            // Check if the current fruit matches the next two in the row.
            if (gridVector[ind2][ind]->fruitNum != NO_FRUIT &&
                gridVector[ind2][ind]->fruitNum ==
                gridVector[ind2+1][ind]->fruitNum &&
                gridVector[ind2+1][ind]->fruitNum ==
                gridVector[ind2+2][ind]->fruitNum) {

                // Check if the 'matched' vector already contains the square.
                for (int ind3 = ind2; ind3 <= ind2+2; ind3++) {
                    // First, lets assume that no duplicates exist.
                    bool matchedContains = false;

                    // Comparing to the contents of the 'matched' vector.
                    for (int ind4 = 0; ind4 < (int)matched.size(); ind4++) {
                        // Match is found, moving to the next fruit square.
                        if(matched[ind4]->id == gridVector[ind3][ind]->id) {
                            matchedContains = true;
                            break;
                        }
                    }
                    // Duplicate not found, adding the current fruit square.
                    if (!matchedContains) {
                        matched.push_back(gridVector[ind3][ind]);
                    }
                }
                // Vertical matches were found.
                matchesFound = true;
            }
        }
    }
    // In the case that the function was called with the intent to remove.
    if (matchesFound && enableRemove) {
        // Removing fruits.
        removeFruits(matched);
    }
    // Returning bool whether matches were found or not.
    return matchesFound;
}

void MainWindow::removeFruits(std::vector<std::shared_ptr<fruitSquare>>&
                              matched) {
    // Scoreboard flashes green when matches are detected (if delay is on)
    ui->scoreLcdNumber->setStyleSheet("background-color: rgb(227, 245, 173);");

    // Updating player points total with new values.
    // Number of total matches determines the points added.
    if ((int)matched.size() == 3) {
        player->points = player->points + THREE_MATCHES_BONUS;
    }
    else if ((int)matched.size() == 4) {
        player->points = player->points +  FOUR_MATCHES_BONUS;
    }
    else if ((int)matched.size() == 5) {
        player->points = player->points +  FIVE_MATCHES_BONUS;
    }
    else if ((int)matched.size() >  5) {
        player->points = player->points +  MAX_BONUS;
    }
    // Making the updated score visible to the player
    ui->scoreLcdNumber->display(player->points);

    // Traversing through the vector containing the matched squares
    for (int ind = 0; ind < (int) matched.size(); ind++) {
        // "Removed" squares get marked as NO_FRUIT. Empty Icon is drawn.
        matched[ind]->fruitButton->setIcon(QIcon());
        matched[ind]->fruitNum = NO_FRUIT;
    }

    bool dropNecessary = true;
    // Checking to see if a drop is necessary, important for intuitive timings
    for(int ind2 = 0; ind2 < COLUMNS - 2; ind2++) {
        if (gridVector[0][ind2]->fruitNum == NO_FRUIT &&
            gridVector[0][ind2 + 1]->fruitNum == NO_FRUIT &&
            gridVector[0][ind2 + 2]->fruitNum == NO_FRUIT &&
            matched.size() <= 4) {
            dropNecessary = false;
        }
    }
    // Checking to see if a drop is necessary, important for intuitive timings
    for(int ind = 0; ind < COLUMNS; ind++) {
        if (gridVector[0][ind]->fruitNum == NO_FRUIT &&
            gridVector[1][ind]->fruitNum == NO_FRUIT &&
            gridVector[2][ind]->fruitNum == NO_FRUIT &&
            matched.size() <= 4) {
            dropNecessary = false;
        }
    }


    // 'Add Delay' and 'Fill Empty' are checked, delaying before dropping.
    if (player->fillEmpty && player->addDelay) {
        if (dropNecessary) {
            QTimer::singleShot(500, this, SLOT(waitDrop()));
        }
        // If a drop isn't necessary, we skip it to keep timings intuitive.
        else if (!dropNecessary){
            QTimer::singleShot(500, this, SLOT(waitRepopulate()));
        }
    }
    // 'Add Delay' is checked but 'Fill Empty' is not, drop is never skipped.
    else if (player->addDelay && !player->fillEmpty) {
        QTimer::singleShot(500, this, SLOT(waitDrop()));
    }
    // Fruits are dropped immediately.
    else {
        dropFruits();
    }
}

void MainWindow::dropFruits() {
    // Settings scoreboard back to original color
    ui->scoreLcdNumber->setStyleSheet("background-color: rgb(252, 175, 62);");

    // Starting from the first column on the left, COLUMNS is the limit.
    for (int ind = 0; ind < COLUMNS; ind++) {

        // Last row of the current column is set as the second  index
        int ind2 = ROWS - 1;

        // Traversing through the column up until first row is reached.
        while (ind2 >= 0) {

            // If the current square has a fruit, moving to the next index.
            if (gridVector[ind2][ind]->fruitNum != NO_FRUIT){
            ind2--;
            }
            else {
                // Setting the column above index 2 as the third index.
                int ind3 = ind2 - 1;
                // Going up the column, looking for the next existing fruit.
                while (ind3 >= 0) {

                    // Fruit found, moving this fruit to the earlier square.
                    if (gridVector[ind3][ind]->fruitNum != NO_FRUIT) {
                        // Drawing fruit to the original square.
                        gridVector[ind2][ind]->fruitNum =
                            gridVector[ind3][ind]->fruitNum;
                        drawFruit(gridVector[ind2][ind]->fruitButton,
                                  gridVector[ind2][ind]->fruitNum);

                        // Square we took the fruit from is marked as empty.
                        gridVector[ind3][ind]->fruitButton->setIcon(QIcon());
                        gridVector[ind3][ind]->fruitNum = NO_FRUIT;

                        // Moving back to the outer loop straight away.
                        break;
                    }
                    ind3--;
                }
            ind2--;
            }
        }
    }
    // Repopulating the board, with or without delay. Board is checked after.
    if(player->fillEmpty) {
        if (player->addDelay) {
            QTimer::singleShot(500, this, SLOT(waitRepopulate()));
        }
        else {
            repopulateGrid();
        }
    }
    // If 'Fill empty' isn't checked, we check for new matches here NOW.
    else {
        // Check-->remove-->drop loop continues until no more matches occur.
        bool matches = checkForMatches(true);
        if (matches) {
        }
    }
}

void MainWindow::repopulateGrid() {
    // Settings scoreboard back to original color
    ui->scoreLcdNumber->setStyleSheet("background-color: rgb(252, 175, 62);");

    // Traversing through the board row by row, column by columnn.
    for (int ind = 0; ind < ROWS; ind++) {

        for (int ind2 = 0; ind2 < COLUMNS; ind2++) {
            // Empty square found, adding new random fruit.
            if (gridVector[ind][ind2]->fruitNum == NO_FRUIT) {
                int randomFruit = distr_(randomEng_);
                gridVector[ind][ind2]->fruitNum = randomFruit;
                drawFruit(gridVector[ind][ind2]->fruitButton, randomFruit);
            }
        }
    }
    // Checking again for matches with the newly repopulated board.
    // Check-->remove-->drop-->repopulate loop continues until
    // no more matches occur.
    bool matches = checkForMatches(true);
    if (matches) {
    }
}

void MainWindow::enableFruitButtons(bool enable) {

    // Traversing through the board while setting all the fruit buttons
    // as disabled or enabled, depending on the parameter.
    for(int ind = 0; ind < ROWS; ind++) {
        for(int ind2 = 0; ind2 < COLUMNS; ind2++) {
            gridVector[ind][ind2]->fruitButton->setEnabled(enable);
        }
    }
}

/*
 * SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW
 * SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW
 * SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW
 * SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW SLOTS BELOW
 */

void MainWindow::onfruitButtonClicked() {
    // Getting the cursor position in relation to the grid/board.
    QPoint windowPos = ui->graphicsView->mapFromGlobal(cursor->pos());

    // Translating the mouse coordinates to rows and columns of the board.
    // Mouse coordinates are turned into small integers by diving them
    // with the square side length and applying the floor function.
    int x = floor ((windowPos.rx()) / SQUARE_SIDE);
    qDebug() << windowPos;
    qDebug() << "x:" << x;
    int y = floor ((windowPos.ry()) / SQUARE_SIDE);
    qDebug() << "y:" << y;

    // Making sure that clicking an empty square does nothing.
    if (gridVector[y][x]->fruitNum == NO_FRUIT) {
        gridVector[y][x]->fruitButton->setChecked(false);
        return;
    }
    // Next we check whether a square has already been clicked (activated).
    if (lastPressed != nullptr)   {
        // Checking if the squares are adjacent to each other.
        bool isAdjacent = checkForAdjacency(lastPressed->posX,
                                            lastPressed->posY, x, y);

        if (isAdjacent && (x <= COLUMNS && 0 <= x && y <= ROWS && 0 <= y)) {
            // Swapping the fruits between previous and current square.
            int tempFruitNum = gridVector[y][x]->fruitNum;
            gridVector[y][x]->fruitNum = lastPressed->fruitNum;
            lastPressed->fruitNum = tempFruitNum;
            drawFruit(gridVector[y][x]->fruitButton,
                      gridVector[y][x]->fruitNum);
            drawFruit(lastPressed->fruitButton,
                      lastPressed->fruitNum);

            // Check whether the new board configuration has new matches.
            bool matchesFound = checkForMatches(true);

            // If no matches were found (and therefore not removed)
            // fruits will be swapped back to their original positions.
            // To the player it looks like the swap was never succesful
            if (!matchesFound) {
                int tempFruitNum = gridVector[y][x]->fruitNum;
                gridVector[y][x]->fruitNum = lastPressed->fruitNum;
                lastPressed->fruitNum = tempFruitNum;
                drawFruit(gridVector[y][x]->fruitButton,
                          gridVector[y][x]->fruitNum);
                drawFruit(lastPressed->fruitButton,
                          lastPressed->fruitNum);
            }
        }
        // After, both fruit buttons are set back to unchecked.
        lastPressed->fruitButton->setChecked(false);
        gridVector[y][x]->fruitButton->setChecked(false);
        // After swapping, there should be no previously checked button.
        lastPressed = nullptr;
    }
    // None of the buttons in the board have been clicked previously.
    else {

        if (x < COLUMNS && 0 <= x && y < ROWS && 0 <= y) {
            // Marking the current button as the last pressed button.
            lastPressed = gridVector[y][x];
            // Saving the location for next time
            lastPressed->posX = x;
            lastPressed->posY = y;
            // Marking the fruit button as checked
            lastPressed->fruitButton->setChecked(true);
        }
    }
}

void MainWindow::primaryTimerFunction() {
    // Retrieving the current time from the lcdNumbers
    double currentSec = ui->lcdNumberSec->value();
    double currentMin = ui->lcdNumberMin->value();

    // When time runs out, a dialog box appears.
    if (currentMin == 0 && currentSec == 0) {

        // New dialog. Setting final points and game over message.
        gameOver = new gameOverDialog(this);
        gameOver->setPoints(player->points);
        gameOver->setReason("Ran out of time.");

        // Resetting game board for next game
        on_resetButton_clicked();
        // Main ui can't be interacted with before dialog is closed.
        gameOver->setModal(true);
        gameOver->show();
    }
    // There's still time left
    else {
        // Current second is 0 (60), next value is set to be 59.
        if (currentSec == 0) {
            currentSec = 59;
            // Subtracting minutes by one.
            currentMin = currentMin - 1;
        }
        // In most cases, current second is subtracted by one.
        else {
            currentSec = currentSec - 1;
        }
    // Updating the new time values to the user.
    ui->lcdNumberMin->display(currentMin);
    ui->lcdNumberSec->display(currentSec);
    }
}

void MainWindow::waitDrop() {
    // Dropping fruits after a delay.
    dropFruits();
}

void MainWindow::waitRepopulate() {
    // Repopulating grid after a delay.
    repopulateGrid();
}

void MainWindow::on_playButton_clicked() {

    // If the timer isn't running, it is set (back) to running.
    if (timer->isActive() == false) {
        timer->start(1000);
    }
    // Pressing the Play button sets itself as not enabled.
    ui->playButton->setEnabled(false);
    // Pause and Give Up buttons are set enabled.
    ui->pauseButton->setEnabled(true);
    ui->giveUpButton->setEnabled(true);

    // Enabling interaction with the game board. Fruits get colorized.
    enableFruitButtons(true);
}

void MainWindow::on_pauseButton_clicked() {

    // If the timer is already running, it is halted.
    if (timer->isActive() == true) {
        timer->stop();
    }
    // Pressing the Pause button sets itself as not enabled.
    ui->pauseButton->setEnabled(false);
    ui->playButton->setEnabled(true);

    // Disabling interaction with the game board. Fruits get greyed out.
    enableFruitButtons(false);
}

void MainWindow::on_giveUpButton_clicked() {

    // Disabling Play button, Enabling play button
    ui->pauseButton->setEnabled(false);
    ui->playButton->setEnabled(true);

    // Initializing new dialog. Setting final points and game over message.
    gameOver = new gameOverDialog(this);
    gameOver->setPoints((int)ui->scoreLcdNumber->value());
    gameOver->setReason("You gave up!");

    // Board gets reset for next round.
    on_resetButton_clicked();

    // Main ui can't be interacted with before dialog is closed.
    gameOver->setModal(true);
    gameOver->show();
}

void MainWindow::on_resetButton_clicked() {

    // Timer is halted if it's active
    if (timer->isActive() == true) {
        timer->stop();
    }
    // Any previously pressed fruit buttons are set back to disabled
    if (lastPressed != nullptr) {
        lastPressed->fruitButton->setChecked(false);
    }
    lastPressed = nullptr;

    // LCDtimers are set back to the original starting values.
    ui->lcdNumberMin->display(3);
    ui->lcdNumberSec->display(0);

    // Disabling interaction with the game board. Fruits get greyed out.
    enableFruitButtons(false);
    // Play button set to enabled, pause and give up to disabled.
    ui->playButton->setEnabled(true);
    ui->pauseButton->setEnabled(false);
    ui->giveUpButton->setEnabled(false);

    // Player points are also reset.
    player->points = 0;
    ui->scoreLcdNumber->display(player->points);

    // Setting new fruits to all the squares, row by row, column by column.
    for (int ind = 0; ind < ROWS; ind++) {

        for (int ind2 = 0; ind2 < COLUMNS; ind2++) {

            int randomFruit = distr_(randomEng_);
            // If the new fruit is the same as two previous,
            // new random fruit is calculated.
            if (ind2 > 1 && randomFruit == gridVector[ind][ind2 - 1]->fruitNum
                && randomFruit == gridVector[ind][ind2 - 2]->fruitNum) {
                    randomFruit = distr_(randomEng_);
                }
            // Drawing the new fruit to the current fruit square.
            gridVector[ind][ind2]->fruitNum = randomFruit;
            drawFruit(gridVector[ind][ind2]->fruitButton, randomFruit);
        }
         // Trying to further reduce the chance of any vertical matches.
        if (ind > 1 && gridVector[ind][ind]->fruitNum ==
            gridVector[ind - 1][ind]->fruitNum &&
            gridVector[ind][ind]-> fruitNum ==
            gridVector[ind - 2][ind]->fruitNum) {

            // Drawing the new fruit to the current fruit square.
            gridVector[ind][ind]-> fruitNum = distr_(randomEng_);
            drawFruit(gridVector[ind][ind]->fruitButton,
                      gridVector[ind][ind]-> fruitNum);
        }

    }
    // The board keeps being reset until we are completely sure that
    // there are no matches of three or more.
    bool matches = checkForMatches(false);
    if (matches) {
        on_resetButton_clicked();
    }
}

void MainWindow::on_delayCheckBox_stateChanged(int arg1) {

    // 'Add Delay' is checked, dropping and repopulation get delayed.
    if (arg1 == 2) {
        player->addDelay = true;
    }
    // 'Add Delay' is unchecked, dropping and repopulation have no delay.
    else {
        player->addDelay = false;
    }
}

void MainWindow::on_fillCheckBox_stateChanged(int arg1) {
    // 'Fill Empty' is checked, empty squares will now get repopulated.
    if (arg1 == 2) {
        player->fillEmpty = true;
        repopulateGrid();
    }
    // 'Fill Empty' is unchecked, empty squares will not be repopulated.
    else {
        player->fillEmpty = false;
    }
}
