/*
#############################################################################
# COMP.CS.110 Programming 2: Autumn 2020                                    #
# Project 4: Match3 (fruit_flip)                                            #
# File: gameoverdialog.hh                                                   #
# Author: Joonas Sorkio (joonas.sorkio@tuni.fi)                             #
# Description: A gameOverDialog is called from mainwindow functions         #
#              if the game over condition is met. Freezes the mainwindow    #
#              until dialog is closed.                                      #
# Notes: * See gameoverdialog.cpp for implementation                        #
#        * ui elements laid out in gameoverdialog.ui                        #
#        * Fruit images provided by Freepik (see license.txt)               #
#############################################################################
*/

#ifndef GAMEOVERDIALOG_HH
#define GAMEOVERDIALOG_HH

#include <QDialog>

// Some constants to assist in printing the results to the user
const QString POINTS_LABEL = "Final points: ";
const QString TIME_LABEL = "Time left: ";

namespace Ui {
class gameOverDialog;
}

class gameOverDialog : public QDialog
{
    Q_OBJECT

public:
    /* Description: Constructor and destructor, created by QT
     * Parameters: parent
     */
    explicit gameOverDialog(QWidget *parent = nullptr);
    ~gameOverDialog();

    /* Description: Sets a new points value.
     * Parameters:
     *  Param1: Final user points from the main ui.
     */
    void setPoints(int newPoints);

    /* Description: Sets a new reason for game ending.
     * Parameters:
     *  Param1: Reason for the game being ended.
     */
    void setReason(QString newReason);

private slots:

    /* Description: Closes the dialog window
     * Parameters: -
     */
    void on_closeButton_clicked();

private:

    Ui::gameOverDialog *ui;

    // Points variable, holds the final points total from the previous game.
    int points = 0;

    // Reason variable, the reason for game being ended is met.
    QString reason = "";
};

#endif // GAMEOVERDIALOG_HH
