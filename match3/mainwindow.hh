/*
#############################################################################
# COMP.CS.110 Programming 2: Autumn 2020                                    #
# Project 4: Match3 (fruit_flip)                                            #
# File: mainwindow.hh                                                       #
# Author: Joonas Sorkio (joonas.sorkio@tuni.fi)                             #
# Description: User interface and functions for the fruit flip game.        #
# Notes: * See mainwindow.cpp for implementation                            #
#        * Game board, fruit squares and corresponding buttons are laid out #
#          manually.                                                        #
#        * Other user interface elements are laid out in mainwindow.ui.    #
#        * Fruit images provided by Freepik (see license.txt)               #
#############################################################################
*/

#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <gameoverdialog.hh>

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <vector>
#include <deque>
#include <random>
#include <memory>
#include <QTimer>
#include <QLabel>
#include <QPushButton>
#include <QCursor>
#include <QDialogButtonBox>

// Constant integer for marking an empty square (no fruit is drawn).
const int NO_FRUIT = -1;

// Constants for different score outputs.
const int THREE_MATCHES_BONUS = 80;
const int FOUR_MATCHES_BONUS = 130;
const int FIVE_MATCHES_BONUS = 260;
const int MAX_BONUS = 350;

// Holds some the information regarding player preferences.
struct Player{
    // Controls whether the empty squares get repopulated during the game.
    bool fillEmpty = true;
    // Player can also choose to add a delay to dropping (and repopulation).
    bool addDelay = true;
    // Total points for the current game.
    int points = 0;
};

// Data concerning individual squares is put into a struct.
struct fruitSquare{
     // Number of the fruit, controls which fruit is displayed on the button.
    int fruitNum = NO_FRUIT;

    // Square position data (in the grid vector), is utilized only
    // when marking the last pressed button.
    int posX = 0;
    int posY = 0;
    // Individual ID for each square, it is used for quick comparison so
    // that matched squares don't get checked as being matched several times
    // thus skewing the scoring output.
    int id = 0;
    // Each square has its own pushbutton to allow for clicking operations.
    QPushButton* fruitButton = nullptr;
};

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /* Description: Constructor and destructor, created by QT
     * Parameters: parent
     */
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

/* Note! I had to use two separate naming conventions for the slots as
 * QT gave warnings for the slot not being signalled properly.
 * For example using "on_fruitButton_clicked()" gave a mysterious warning.
 * All the slots and functions created manually follow the CamelCase.
 */
private slots:

    /* Description: Slot for fruitbuttons(squares) being clicked.
     *  When any fruit button is clicked, the location data of the
     *  cursor is translated into the (2D) grid vector coordinates and
     *  calculations are made accordingly.
     * Parameters: -
     */
    void onfruitButtonClicked();

    /* Description: Timer operations concerning the timer visible in the
     *  user interface. Keeps count of the game time left. (min and sec).
     * Parameters: -
     */
    void primaryTimerFunction();

    /* Description: Slot for delaying the fruit dropping operations,
     *  Is utilized only when 'Add delay' is checked in the UI.
     * Parameters: -
     */
    void waitDrop();

    /* Description: Slot for delaying the refilling the grid,
     *  Is utilized only when 'Add Delay' is checked in the UI.
     *  Is utilized only when 'Fill Empty' is checked in the UI.
     * Parameters: -
     */
    void waitRepopulate();

    /* Description: Slot for 'Play' button operations. Starts the timer
     *  and enables the fruit buttons. Enables the use of
     *  'Pause' and 'Give up' - buttons.
     * Parameters: -
     */
    void on_playButton_clicked();

    /* Description: Slot for 'Pause' button operations. Stops the timer
     *  and disables the fruit buttons. Enables the use of
     *  'Play' button.
     * Parameters: -
     */
    void on_pauseButton_clicked();

    /* Description: Slot for 'Give Up' button operations.
     *  Game board is reset for the next game and the final score is printed
     *  to the user via gameoverdialog.
     * Parameters: -
     */
    void on_giveUpButton_clicked();

    /* Description: Slot for 'Reset' button operations. Assings new fruits to
     *  all fruit squares and the score and the 'time left'are both reset.
     *  Can be called for several times in a row (and even from fillGrid) in
     *  the case that the game board initially contains any matches.
     *  On top of that some operations within the loop structure also attempt
     *  to prevent this and thus reduce unnecessary extra calls.
     *  Fruit buttons are set to disabled, until the user clicks 'Play'.
     *  Disables the use of 'Pause' and 'Give up' - buttons.
     * Parameters: -
     */
    void on_resetButton_clicked();

    /* Description: Slot for 'Add Delay' button operations.
     *  User can control whether dropping and repopulation operations
     *  have delay. Is checked by default.
     * Parameters: New checkbox state (2 or 0)
     */
    void on_delayCheckBox_stateChanged(int arg1);

    /* Description: Slot for 'Fill Empty' button operations.
     *  User can control whether the empty squares are refilled automatically.
     *  Is checked by default.
     * Parameters: New checkbox state (2 or 0)
     */
    void on_fillCheckBox_stateChanged(int arg1);


private:

    Ui::MainWindow *ui;

    // Scene for the game grid
    QGraphicsScene* scene_;
    // Timer for keeping count of the time left/elapsed.
    QTimer* timer;
    // Cursor, used for getting cursor location data in the game board.
    QCursor* cursor;
    // Dialog to notify the user of the game ending. See gameoverdialog.hh
    gameOverDialog* gameOver;

    // Margins for the graphicsview
    const int TOP_MARGIN = 20;
    const int LEFT_MARGIN = 20;

    // Default size of a fruit square.
    const int SQUARE_SIDE = 50;
    // Number of vertical cells (places for fruits).
    const int ROWS = 8;
    // Number of horizontal cells (places for fruits).
    const int COLUMNS = 13;

    // Constants describing scene coordinates.
    const int BORDER_UP = 0;
    const int BORDER_DOWN = SQUARE_SIDE * ROWS;
    const int BORDER_LEFT = 0;
    const int BORDER_RIGHT = SQUARE_SIDE * COLUMNS;
    // Number of the different fruits utilized.
    const int NUMBER_OF_FRUITS = 8;

    // Random engine and distribution used for fruit printing.
    std::default_random_engine randomEng_;
    std::uniform_int_distribution<int> distr_;


    /* Description: Draws an icon of a fruit to the corresponding fruit button.
     *  In the assignment, use of QLabel was recommended but the main teacher
     *  (Maarit Harsu) said utilizing QIcons is also OK.
     * Parameters:
     *  Param1: The fruit button to display the fruit on.
     *  Param2: Number of the fruit to be displayed. Nine possible fruits.
     */
    void drawFruit(QPushButton* fruitButton, int fruitNum);

    /* Description: Filling the scene with fruit squares (and push buttons).
     *  Shared pointers to individual fruit squares are saved into a
     *  two dimensional vector (gridVector). If the board contains
     *  matches (of fruits) reset function is called as many times as
     *  necessary.
     * Parameters: -
     */
    void fillGrid();

    /* Description: Checks to see whether the squares user is attempting
     *  to swap are adjacent to each other(horizontally/vertically) and
     *  thus swappable.
     * Parameters:
     *  Param1: Row of the previously clicked fruit square.
     *  Param2: Column of the previously clicked fruit square.
     *  Param3: Row of the next clicked fruit square.
     *  Param4: Column of the next clicked fruit square.
     * Returns: A bool value 'true' if the squares are swappable (by proximity),
     *          'false' if not.
     */
    bool checkForAdjacency(int& lastX, int& lastY, int& newX, int& newY);

    /* Description: Checks to see whether the proposed board configuration
     *  contains any matches of three or more. Can be used to check if the
     *  attempted swap by the user is valid as the game only allows swaps
     *  that lead to matches. Probably the most utilized function of the
     *  whole program. If 'enableRemove' parameter is 'true', removeFruits
     *  is also called.
     * Parameters:
     *  Param1: Bool value that controls the removal of matched squares.
     * Returns: A bool value 'true' if matches of 3 or more are found,
     *          'false' if not.
     */
    bool checkForMatches(bool enableRemove);

    /* Description: 'Removes' the chosen fruits from the boards.
     *  Actual squares and their buttons arent removed, the square is simply
     *  marked as NO_FRUIT and an empty icon is drawn to the matching button.
     *  Is called only from checkForMatches. After removal, dropFruits
     *  is called immediately or with delay(player preference).
     * Parameters:
     *  Param1: Vector containing pointers to the squares about to be removed.
     */
    void removeFruits(std::vector<std::shared_ptr<fruitSquare>>& matched);

    /* Description: Remaining fruits are dropped down so that any empty spaces
     *  (vertically) betwween the fruits are removed. If the player has
     *  checked 'Fill Empty' in the UI, the board is also repopulated after.
     *  See repopulateGrid.
     * Parameters: -
     */
    void dropFruits();

    /* Description: Fills all the empty spaces remaining with new fruits
     *  if the 'Fill Empty' checkbox is checked in the UI. Is only called
     *  after the existing fruits have been dropped down.
     *  'Add Delay' option also affects this function.
     * Parameters: -
     */
    void repopulateGrid();

    /* Description: Enables or disables the fruit buttons on the board
     *  Is used, extensively in play, pause and reset functions for example.
     * Parameters: -
     */
    void enableFruitButtons(bool enable);

    // Shared pointer to the last clicked (or pressed) fruit square.
    std::shared_ptr<fruitSquare> lastPressed = nullptr;

    // 2D-Vector into which pointers to all the fruit squares are added
    std::vector<std::vector<std::shared_ptr<fruitSquare>>> gridVector = {};

    // Pointer to the Player struct containing user settings and points.
    std::shared_ptr<Player> player = nullptr;
};
#endif // MAINWINDOW_HH
