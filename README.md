# fruit-flipperino

A Match 3 fruit game. 
Created as a course project for TUNI C++ programming course "Programming 2: Structures". 
User interface and comments are only in English. 
Originally created on Qt Creator 5.14.2 on Linux.
See instructions.txt